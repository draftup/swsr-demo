// @flow

if (navigator.serviceWorker !== undefined) {
  navigator.serviceWorker
    // $FlowIgnore
    .register(new URL("service-worker.js", import.meta.url), {
      type: "module",
    })
    .then((reg) => {
      console.log(`Registration succeeded. Scope is ${reg.scope}`);

      window.document.body.innerHTML = "<h1>Ready</h1>";
    })
    .catch((error) => {
      console.log(`Registration failed with ${error}`);
    });
}
