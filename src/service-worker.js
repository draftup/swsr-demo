// @flow

import { manifest, version } from "@parcel/service-worker";

console.log(">> service worker");

async function install() {
  console.log(">> install");
  const cache = await caches.open(version);
  await cache.addAll(manifest);
}
// eslint-disable-next-line no-restricted-globals
self.addEventListener("install", (e) => e.waitUntil(install()));

async function activate() {
  console.log(">> activate");
  const keys = await caches.keys();
  await Promise.all(keys.map((key) => key !== version && caches.delete(key)));
}
// eslint-disable-next-line no-restricted-globals
self.addEventListener("activate", (e) => e.waitUntil(activate()));

// eslint-disable-next-line no-restricted-globals
self.addEventListener("fetch", (event) => {
  console.log(">> fetch", event);

  //   if (event.request.mode === "navigate") {
  //     const headers = new Headers();
  //     headers.append("Content-Type", "text/html");

  //     const document = `
  //     <!DOCTYPE html>
  //     <html lang="en">
  //         <head>
  //             <meta charset="utf-8" />
  //             <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  //             <title>Service worker page</title>
  //         </head>
  //         <body>
  //             <h1>This page was created on service sorker side</h1>
  //         </body>
  //     </html>
  // `;

  //     const response = new Response(document, {
  //       headers,
  //     });

  //     event.respondWith(response);
  //   }

  event.respondWith(fetch(event.request));
});
