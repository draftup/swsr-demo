declare type ServiceWorkerGlobalScope = {
  [key: string]: any,
};
